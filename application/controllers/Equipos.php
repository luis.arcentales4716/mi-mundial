<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Equipos extends CI_Controller
{

	// definiendo el constructor de la clase 
	public function __construct()
	{
		parent::__construct();
		$this->load->model("equipo"); //llamamos al modeelo 
	}


	public function index()
	{
		$data["listadoEquipos"] = $this->equipo->obtenerTodos(); //se hace la consulta
		$this->load->view('header');
		$this->load->view('equipos/index', $data);
		$this->load->view('footer');
	}
	
	// funcion para capturar los valores del formulario nuevo 


	public function guardarEquipo()
	{
		$datosNuevoEquipo = array(
			//posicion dentreo del array 
			"nombre_equ_fs" => $this->input->post('nombre_equ_fs'),
			"continente_equ_fs" => $this->input->post('continente_equ_fs')
		);

		//Inicio del proceso de subida de archivos 
		//$this->load->library("upload"); //Activando la libreria de subida de archivos
		//$new_name = "foto_" . time() . "_" . rand(1, 5000);  //Generando un nombre aleatorio
		//$config['file_name'] = $new_name;   //
		//$config['upload_path'] = FCPATH . 'uploads/estudiantes'; //ruta a donde subir el archivo 
		//$config['allowed_types'] = 'png|jepg|jpg'; //dos extensiones especificas
		//$config['max_size'] = 2*1024; //2 Megabytes
		//$this->upload->initialize($config);  //inicializar la configuraicon
		//Validando la subida de archivo 
		//if ($this->upload->do_upload("foto_est")) {
			//Representa que si se subio con exito 
			//$dataSubida = $this->upload->data();
			//$datosNuevoEstudiante["foto_est"] = $dataSubida['file_name'];
		//}
		//else{
			//print_r($this->upload->display_errors());  //Imprimir los errores al salir del condicional 
		//}

		//Fin del proceso de subida de archivos 


		print_r($datosNuevoEquipo);
		if ($this->equipo->insertar($datosNuevoEquipo)) {

			redirect('equipos/index');
		} else {
			echo "<h1>Error</h1>";
		}

	}

	public function borrar($id_equ_fs)
	{
		if ($this->equipo->eliminarPorId($id_equ_fs)) {
			echo "Eliminado";
			redirect('equipos/index');

		} else {
			echo "Error al eliminar :c ";
		}
	}


	public function actualizar($id)
	{
		$data["equipoEditar"] = $this->equipo->obtenerPorId($id);
		$this->load->view('header');
		$this->load->view('equipos/actualizar', $data);
		$this->load->view('footer');


	}
	// public function procesarActualizacion()
	// {
	// 	// error_reporting(0);
	// 	//desactivar los errores 
	// 	$datosEstudianteEditado = array(
	// 		//posicion dentreo del array 
	// 		"cedula_est" => $this->input->post('cedula_est'),
	// 		"apellido_est" => $this->input->post('apellido_est'),
	// 		"nombre_est" => $this->input->post('nombre_est'),
	// 		"telefono_est" => $this->input->post('telefono_est'),
	// 		"direccion_est" => $this->input->post('direccion_est'),
	// 		"fecha_nacimiento_est" => $this->input->post('fecha_nacimiento_est')
	// 	);
	// 	$id=$this->input->post("id_est");
	// 	if ($this->estudiante->actualizar($id,$datosEstudianteEditado)) {

	// 		redirect('estudiantes/index');
	// 	} else {
	// 		echo "<h1>Error</h1>";
	// 	}
	// }

	public function procesarActualizacion()
	{
		$datosEquipoEditado = array(
			"nombre_equ_fs" => $this->input->post('nombre_equ_fs'),
			"continente_equ_fs" => $this->input->post('continente_equ_fs'),
		);
		$id = $this->input->post("id_equ_fs");
		if ($this->equipo->actualizar($id, $datosEquipoEditado)) {
			redirect('equipos/index');
		} else {
			echo "<h1>ERROR</h1>";
		}
	}

}