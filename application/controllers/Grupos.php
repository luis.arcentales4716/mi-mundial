<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Grupos extends CI_Controller
{

	// definiendo el constructor de la clase 
	public function __construct()
	{
		parent::__construct();
		$this->load->model("grupo"); //llamamos al modeelo 
	}


	public function index()
	{
		$data["listadoGrupos"] = $this->grupo->obtenerTodos(); //se hace la consulta
		$this->load->view('header');
		$this->load->view('grupos/index', $data);
		$this->load->view('footer');
	}
	
	// funcion para capturar los valores del formulario nuevo 


	public function guardarGrupo()
	{
		$datosNuevoGrupo = array(
			//posicion dentreo del array 
			"nombre_gru_fs" => $this->input->post('nombre_gru_fs'),
			
		);

		//Inicio del proceso de subida de archivos 
		//$this->load->library("upload"); //Activando la libreria de subida de archivos
		//$new_name = "foto_" . time() . "_" . rand(1, 5000);  //Generando un nombre aleatorio
		//$config['file_name'] = $new_name;   //
		//$config['upload_path'] = FCPATH . 'uploads/estudiantes'; //ruta a donde subir el archivo 
		//$config['allowed_types'] = 'png|jepg|jpg'; //dos extensiones especificas
		//$config['max_size'] = 2*1024; //2 Megabytes
		//$this->upload->initialize($config);  //inicializar la configuraicon
		//Validando la subida de archivo 
		//if ($this->upload->do_upload("foto_est")) {
			//Representa que si se subio con exito 
			//$dataSubida = $this->upload->data();
			//$datosNuevoEstudiante["foto_est"] = $dataSubida['file_name'];
		//}
		//else{
			//print_r($this->upload->display_errors());  //Imprimir los errores al salir del condicional 
		//}

		//Fin del proceso de subida de archivos 


		print_r($datosNuevoGrupo);
		if ($this->grupo->insertar($datosNuevoGrupo)) {

			redirect('grupos/index');
		} else {
			echo "<h1>Error</h1>";
		}

	}

	public function borrar($id_gru_fs)
	{
		if ($this->grupo->eliminarPorId($id_gru_fs)) {
			echo "Eliminado";
			redirect('grupos/index');

		} else {
			echo "Error al eliminar :c ";
		}
	}


	public function actualizar($id)
	{
		$data["grupoEditar"] = $this->grupo->obtenerPorId($id);
		$this->load->view('header');
		$this->load->view('grupos/actualizar', $data);
		$this->load->view('footer');


	}
	// public function procesarActualizacion()
	// {
	// 	// error_reporting(0);
	// 	//desactivar los errores 
	// 	$datosEstudianteEditado = array(
	// 		//posicion dentreo del array 
	// 		"cedula_est" => $this->input->post('cedula_est'),
	// 		"apellido_est" => $this->input->post('apellido_est'),
	// 		"nombre_est" => $this->input->post('nombre_est'),
	// 		"telefono_est" => $this->input->post('telefono_est'),
	// 		"direccion_est" => $this->input->post('direccion_est'),
	// 		"fecha_nacimiento_est" => $this->input->post('fecha_nacimiento_est')
	// 	);
	// 	$id=$this->input->post("id_est");
	// 	if ($this->estudiante->actualizar($id,$datosEstudianteEditado)) {

	// 		redirect('estudiantes/index');
	// 	} else {
	// 		echo "<h1>Error</h1>";
	// 	}
	// }

	public function procesarActualizacion()
	{
		$datosGrupoEditado = array(
			"nombre_gru_fs" => $this->input->post('nombre_gru_fs'),
			
		);
		$id = $this->input->post("id_gru_fs");
		if ($this->grupo->actualizar($id, $datosGrupoEditado)) {
			redirect('grupos/index');
		} else {
			echo "<h1>ERROR</h1>";
		}
	}

}