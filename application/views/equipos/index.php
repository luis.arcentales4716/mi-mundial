<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <br>
    <h2 class="well text-center">Gestion de Equipos</h2>

    <div class="row">
        <div class="col-md-6 text-center">

            <form id="frm_nuevo_equipo" class="" enctype="multipart/form-data" action="<?php echo site_url("equipos/guardarEquipo"); ?>" method="post">
                <!-- utilziar metodo post para mas seguridad  -->
                <!-- enctype lleva multiples archivos -->
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">NOMBRE:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese elnombre de la seleccion" name="nombre_equ_fs" id="nombre_equ_fs">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">CONTINENTE:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese el continente" name="continente_equ_fs" id="continente_equ_fs">
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    <button type="submit" name="button" class="btn btn-primary">
                        Guardar
                    </button>
                    <a href="<?php echo site_url("equipos/index"); ?>" class="btn btn-danger">
                        Cancelar
                    </a>
                </div>
            </form>
            <br>
            <script type="text/javascript">
                $(document).ready(function() { //se solicita validacion cuando el dom se carga 
                    $('#frm_nuevo_equipo').validate({
                        rules: {
                            nombre_equ_fs: {
                                required: true,
                                minlength: 3
                            },
                            continente_equ_fs: {
                                required: true,
                                minlength: 10,
                                maxlength: 13,
                                digits: true
                            },

                        },
                        messages: {
                            nombre_equ_fs: {
                                required: "Porfavor complete los Apellidos",
                                minlength: "Apellido Incorrecto"
                            },
                            continente_equ_fs: {
                                required: "Porfavor complete su Cedula",
                                minlength: "Cedula/RUC Incorrecto",
                                maxlength: "Cedula/RUC Incorrecto",
                                digits: "Este campo solo acepta numeros"

                            },

                        }
                    });
                    // se captura el dato por id
                });
            </script>
            <script>
                const input = document.querySelector('input');
                const preview = document.querySelector('.preview');


                // input.style.opacity = 0;
            </script>
        </div>
        <div class="col-md-6 text-center container">
            <table id="tbl_equipos" class=" table table-stripped table-bordered table-hover">
                <thead>
                    <th class="text-center">ID</th>
                    <th class="text-center">NOMBRE</th>
                    <th class="text-center">CONTINENTE</th>
                    <th class="text-center">Acciones</th>
                </thead>
                <tbody>
                    <?php if ($listadoEquipos) : ?>
                        <?php foreach ($listadoEquipos->result() as $equipoTemporal) : ?>
                            <tr>
                                <td class="textcenter"><?php echo $equipoTemporal->id_equ_fs; ?></td>
                                <td class="textcenter"><?php echo $equipoTemporal->nombre_equ_fs; ?></td>
                                <td class="textcenter"><?php echo $equipoTemporal->continente_equ_fs; ?></td>
                                <td class="textcenter">
                                    <a class="btn btn-primary glyphicon glyphicon-pencil" tooltip="sa" href="<?php echo site_url("equipos/actualizar"); ?>/<?php echo $equipoTemporal->id_equ_fs; ?>"></a>
                                    <a onclick="return confirm('Esta seguro de eliminar?')" class="btn btn-danger glyphicon glyphicon-trash" href="<?php echo site_url("equipos/borrar"); ?>/<?php echo $equipoTemporal->id_equ_fs; ?>"></a>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <h3 class="text-center">No existen equipos </h3>
                    <?php endif; ?>


                </tbody>

            </table>
        </div>

        <script type="text/javascript">
            $('#tbl_equipos').DataTable();
        </script>
        <style>
            .tr {
                background-color: aquamarine;
            }
        </style>
    </div>
    </div>
</body>

</html>