<div class="row">
    <div class="col-md-12 text-center well">
        <h3>Actualizar Equipo</h3>
    </div>
</div>
<div class="text-center">
    <a href="<?php echo site_url("equipos/index"); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-menu-left"></i>
        Volver
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($equipoEditar) : ?>
            <form action="<?php echo site_url("equipos/procesarActualizacion"); ?>" method="post">


                <center>
                    <input value="<?php echo $equipoEditar->id_equ_fs; ?>" type="hidden" name="id_equ_fs" method="post">

                </center>


                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">NOMBRE:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese número de cédula" name="nombre_equ_fs" value="<?php echo $equipoEditar->nombre_equ_fs; ?>" required>
                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">CONTINENETE:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese los dos apellidos completos" name="continente_equ_fs" value="<?php echo $equipoEditar->continente_equ_fs; ?>" required>
                    </div>
                </div>
                <br>
                <div class="row text-center">
                    <div class="col-md-12 text-center">
                        <button type="submit" name="button" class="btn btn-warning">
                            Actualizar
                        </button>
                        <a href="<?php echo site_url("equipos/index"); ?>" class="btn btn-danger">
                            Cancelar
                        </a>
                    </div>
                    <br>
            </form>
        <?php else : ?>
            <div class="alert alert-danger">
                <b>No se encontro al equipo</b>

            </div>
        <?php endif; ?>
    </div>
</div>