<div class="row">
    <div class="col-md-12 text-center well">
        <h3>Actualizar Grupo</h3>
    </div>
</div>
<div class="text-center">
    <a href="<?php echo site_url("grupos/index"); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-menu-left"></i>
        Volver
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($grupoEditar) : ?>
            <form action="<?php echo site_url("grupos/procesarActualizacion"); ?>" method="post">


                <center>
                    <input value="<?php echo $grupoEditar->id_gru_fs; ?>" type="hidden" name="id_gru_fs" method="post">

                </center>


                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">NOMBRE:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese número de cédula" name="nombre_gru_fs" value="<?php echo $grupoEditar->nombre_gru_fs; ?>" required>
                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
                <br>
                <div class="row text-center">
        <div class="col-md-12 text-center">
                        <button type="submit" name="button" class="btn btn-warning">
                            Actualizar
                        </button>
                        <a href="<?php echo site_url("grupos/index"); ?>" class="btn btn-danger">
                            Cancelar
                        </a>
                    </div>
                    <br>
            </form>
        <?php else : ?>
            <div class="alert alert-danger">
                <b>No se encontro al grupo</b>

            </div>
        <?php endif; ?>
    </div>
</div>