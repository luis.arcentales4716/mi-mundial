<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <br>
    <h2 class="well text-center">Gestinar Calendarios de Encuntros</h2>

    <div class="row">
        <div class="col-md-6 text-center">

            <form id="frm_nuevo_calendario" class="" enctype="multipart/form-data" action="<?php echo site_url("calendarios/guardarCaledario"); ?>" method="post">
                <!-- utilziar metodo post para mas seguridad  -->
                <!-- enctype lleva multiples archivos -->
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">LOCAL:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese elnombre de la seleccion" name="rival1_cal_fs" id="rival1_cal_fs">
                    </div>
                </div>
                
                <h6>VS</h6>
                
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">VISITANTE:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese el continente" name="rival2_cal_fs" id="rival2_cal_fs">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">ESTADIO:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese el continente" name="estadio_cal_fs" id="estadio_cal_fs">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">FECHA:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="date" class="form-control " placeholder="Ingrese el continente" name="fecha_cal_fs" id="fecha_cal_fs">
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    <button type="submit" name="button" class="btn btn-primary">
                        Guardar
                    </button>
                    <a href="<?php echo site_url("calendarios/index"); ?>" class="btn btn-danger">
                        Cancelar
                    </a>
                </div>
            </form>
            <br>
            <script type="text/javascript">
                $(document).ready(function() { //se solicita validacion cuando el dom se carga 
                    $('#frm_nuevo_equipo').validate({
                        rules: {
                            nombre_equ_fs: {
                                required: true,
                                minlength: 3
                            },
                            continente_equ_fs: {
                                required: true,
                                minlength: 10,
                                maxlength: 13,
                                digits: true
                            },

                        },
                        messages: {
                            nombre_equ_fs: {
                                required: "Porfavor complete los Apellidos",
                                minlength: "Apellido Incorrecto"
                            },
                            continente_equ_fs: {
                                required: "Porfavor complete su Cedula",
                                minlength: "Cedula/RUC Incorrecto",
                                maxlength: "Cedula/RUC Incorrecto",
                                digits: "Este campo solo acepta numeros"

                            },

                        }
                    });
                    // se captura el dato por id
                });
            </script>
            <script>
                const input = document.querySelector('input');
                const preview = document.querySelector('.preview');


                // input.style.opacity = 0;
            </script>
        </div>
        <div class="col-md-6 text-center">
            <table id="tbl_equipos" class=" table table-stripped table-bordered table-hover">
                <thead>
                    <th class="text-center">ID</th>
                    <th class="text-center">LOCAL</th>
                    <th class="text-center">VISITANTE</th>
                    <th class="text-center">ESTADIO</th>
                    <th class="text-center">FECHA</th>
                    <th class="text-center">ACCIONES</th>
                </thead>
                <tbody>
                    <?php if ($listadoCalendarios) : ?>
                        <?php foreach ($listadoCalendarios->result() as $calendarioTemporal) : ?>
                            <tr>
                                <td class="textcenter"><?php echo $calendarioTemporal->id_cal_fs; ?></td>
                                <td class="textcenter"><?php echo $calendarioTemporal->rival1_cal_fs; ?></td>
                                <td class="textcenter"><?php echo $calendarioTemporal->rival2_cal_fs; ?></td>
                                <td class="textcenter"><?php echo $calendarioTemporal->estadio_cal_fs; ?></td>
                                <td class="textcenter"><?php echo $calendarioTemporal->fecha_cal_fs; ?></td>
                                
                                <td class="textcenter">
                                    <a class="btn btn-primary glyphicon glyphicon-pencil" tooltip="sa" href="<?php echo site_url("calendarios/actualizar"); ?>/<?php echo $calendarioTemporal->id_cal_fs; ?>"></a>
                                    <a onclick="return confirm('Esta seguro de eliminar?')" class="btn btn-danger glyphicon glyphicon-trash" href="<?php echo site_url("calendarios/borrar"); ?>/<?php echo $calendarioTemporal->id_cal_fs; ?>"></a>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <h3 class="text-center">No existen equipos </h3>
                    <?php endif; ?>


                </tbody>

            </table>
        </div>

        <script type="text/javascript">
            $('#tbl_calendarios').DataTable();
        </script>
        <style>
            .tr {
                background-color: aquamarine;
            }
        </style>
    </div>
    </div>
</body>

</html>