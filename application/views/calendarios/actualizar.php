<div class="row">
    <div class="col-md-12 text-center well">
        <h3>Actualizar Calendario</h3>
    </div>
</div>
<div class="text-center">
    <a href="<?php echo site_url("calendarios/index"); ?>" class="btn btn-primary">
        <i class="glyphicon glyphicon-menu-left"></i>
        Volver
    </a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?php if ($calendarioEditar) : ?>
            <form action="<?php echo site_url("calendarios/procesarActualizacion"); ?>" method="post">


                <center>
                    <input value="<?php echo $calendarioEditar->id_cal_fs; ?>" type="hidden" name="id_cal_fs" method="post">

                </center>


                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">LOCAL:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese número de cédula" name="rival1_cal_fs" value="<?php echo $calendarioEditar->rival1_cal_fs; ?>" required>
                    </div>
                    <div class="col-md-4">

                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">VISITANTE:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese los dos apellidos completos" name="rival2_cal_fs" value="<?php echo $calendarioEditar->rival2_cal_fs; ?>" required>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">ESTADIO:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese los dos apellidos completos" name="estadio_cal_fs" value="<?php echo $calendarioEditar->estadio_cal_fs; ?>" required>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="">FECHA:</label>
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control " placeholder="Ingrese los dos apellidos completos" name="fecha_cal_fs" value="<?php echo $calendarioEditar->fecha_cal_fs; ?>" required>
                    </div>
                </div>

    </div>
    <br>
    <div class="row text-center">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-warning">
                Actualizar
            </button>
            <a href="<?php echo site_url("calendarios/index"); ?>" class="btn btn-danger">
                Cancelar
            </a>
        </div>
        <br>
        </form>
    </div>
<?php else : ?>
    <div class="alert alert-danger">
        <b>No se encontro al equipo</b>

    </div>
<?php endif; ?>
</div>
</div>