<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mi mundial</title>

    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!--Importacion de jquery validate -->
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/jquery.validate.min.js'); ?>"></script>
    <!--Iportar-->
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/additional-methods.min.js'); ?>"></script>
    <!--Importar libreria para espanol-->
    <script type="text/javascript" src="<?php echo base_url('assets/librerias/validate/messages_es_AR.min.js'); ?>"></script>

    <!--importaciion de datatables-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <script type="tect/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
</head>

<body>
    <nav style="height:79px; background-color:#020F2A; display: flex; justify-content: space-around;">
        <div style="width: 40%;  display: flex; align-items: center;" class="text-center">
        <img src="<?php echo base_url("assets/img/logomundial.png");?>">
            <ul class="nav navbar-nav">
                
                <li><a href="<?php echo site_url(); ?>" style="color: white;">MI MUNDIAL</a></li>
            </ul>
        </div>
        <div style="width: 50%;  display: flex; align-items: center;" class="text-center">

            <ul class="nav navbar-nav">
                <li><a href="<?php echo site_url(); ?>">INICIO</a></li>
                <li><a href="<?php echo site_url('equipos/index');?>">EQUIPOS</a></li>
                <li><a href="<?php echo site_url('grupos/index');?>">GRUPOS</a></li>
                <li><a href="<?php echo site_url('posiciones/index');?>">POSICIONES</a></li>
                <li><a href="<?php echo site_url('fases/index');?>">FASES</a></li>
                <li><a href="<?php echo site_url('calendarios/index');?>">CALENDARIO</a></li>
            </ul>
        </div>
    </nav>