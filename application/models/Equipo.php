<?php
class Equipo extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function obtenerTodos(){
        $equipos=$this->db->get("equipo");
        if($equipos->num_rows()>0){ //(select * from clientes)cuando si hay clientes
            return $equipos;
        }else{
            return false; //cuando no existen clientes 
        }

    }
    public function insertar($datos){

        return $this->db->insert("equipo",$datos);
    }

    public function eliminarPorId($id){ //id variable para buscar y comparar en db 
        $this->db->where("id_equ_fs",$id);
        return $this->db->delete("equipo");
    }

    //consultando equipo por id 
    public function obtenerPorId($id){
        $this->db->where("id_equ_fs",$id);
        $equipo=$this->db->get("equipo");
        if($equipo->num_rows()>0){
            return $equipo->row(); //devuelve solo el row  porque solo viene uno debido al id
        }else{
            return false;
            
        }
    }//Proceso de actualizacion de equipos 
    public function actualizar($id,$datos){
        $this->db->where("id_equ_fs",$id);  //filtro en donde se va a actualizar 
        return $this->db->update("equipo",$datos); //actualiza los datos dentro de la db 
    }

}


  