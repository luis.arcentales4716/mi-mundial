<?php
class Calendario extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function obtenerTodos(){
        $calendarios=$this->db->get("calendario");
        if($calendarios->num_rows()>0){ //(select * from clientes)cuando si hay clientes
            return $calendarios;
        }else{
            return false; //cuando no existen clientes 
        }

    }
    public function insertar($datos){

        return $this->db->insert("calendario",$datos);
    }

    public function eliminarPorId($id){ //id variable para buscar y comparar en db 
        $this->db->where("id_cal_fs",$id);
        return $this->db->delete("calendario");
    }

    //consultando equipo por id 
    public function obtenerPorId($id){
        $this->db->where("id_cal_fs",$id);
        $calendario=$this->db->get("calendario");
        if($calendario->num_rows()>0){
            return $calendario->row(); //devuelve solo el row  porque solo viene uno debido al id
        }else{
            return false;
            
        }
    }//Proceso de actualizacion de equipos 
    public function actualizar($id,$datos){
        $this->db->where("id_cal_fs",$id);  //filtro en donde se va a actualizar 
        return $this->db->update("calendario",$datos); //actualiza los datos dentro de la db 
    }

}


  