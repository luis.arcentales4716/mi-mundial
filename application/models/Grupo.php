<?php
class Grupo extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function obtenerTodos(){
        $grupos=$this->db->get("grupo");
        if($grupos->num_rows()>0){ //(select * from clientes)cuando si hay clientes
            return $grupos;
        }else{
            return false; //cuando no existen clientes 
        }

    }
    public function insertar($datos){

        return $this->db->insert("grupo",$datos);
    }

    public function eliminarPorId($id){ //id variable para buscar y comparar en db 
        $this->db->where("id_gru_fs",$id);
        return $this->db->delete("grupo");
    }

    //consultando equipo por id 
    public function obtenerPorId($id){
        $this->db->where("id_gru_fs",$id);
        $grupo=$this->db->get("grupo");
        if($grupo->num_rows()>0){
            return $grupo->row(); //devuelve solo el row  porque solo viene uno debido al id
        }else{
            return false;
            
        }
    }//Proceso de actualizacion de grupos 
    public function actualizar($id,$datos){
        $this->db->where("id_gru_fs",$id);  //filtro en donde se va a actualizar 
        return $this->db->update("grupo",$datos); //actualiza los datos dentro de la db 
    }

}


  